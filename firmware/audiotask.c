#include <FreeRTOS.h>
#include <task.h>

#include "audio.h"
#include "usbaudio.h"

#define MIN(a,b) ((a<b) ? a : b)


void 
audiotask() {
  StreamBufferHandle_t iq_stream = audio_iq_stream();
  StreamBufferHandle_t hp_stream = audio_hp_stream();
  StreamBufferHandle_t mic_stream = audio_mic_stream();
  StreamBufferHandle_t usb_stream = usbaudio_stream();
  
  size_t n, m;
  for (;;) {
    if (n = xStreamBufferBytesAvailable(iq_stream)/sizeof(IQSample)) {
      if (m = xStreamBufferSpacesAvailable(usb_stream)/sizeof(IQSample)) {
        for (size_t i=0; i<MIN(n,m); i++) {
          IQSample sample;
          xStreamBufferReceive(iq_stream, &sample, sizeof(IQSample), 0);
          xStreamBufferSend(usb_stream, &sample, sizeof(IQSample), 0);
          xStreamBufferSend(hp_stream, &sample.i, sizeof(int16_t), 0);
        }
      }
    }

    taskYIELD();
  }
}
