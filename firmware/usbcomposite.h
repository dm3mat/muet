/** @defgroup usb USB Interface
 * Functions implementing the USB interface to the host. This interface provides a so-called 
 * composite USB device. That is, it provides two different USB device classes for one device.
 * 
 * The first interface is a USB CDC-ACM device, also known as serial-over-USB. This interface then
 * provides the CAT control for the transceiver.
 * 
 * The second interface is a USB sound device, itself providing two audio interfaces. 
 */
#ifndef __USBCOMPOSITE_H__
#define __USBCOMPOSITE_H__

#include <FreeRTOS.h>
#include <stream_buffer.h>

#define USBSERIAL_EP_RX_ADDR     0x01  //< End-point address of CDC ACM RX stream (incoming data).
#define USBAUDIO_EP_LINEOUT_ADDR 0x84  //< End-point address of USB Line-out stream.

void usbcomposite_setup(void);

#endif