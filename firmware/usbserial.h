#ifndef __USBSERIAL_H__
#define __USBSERIAL_H__

#include <FreeRTOS.h>
#include <stream_buffer.h>

#include <libopencm3/usb/usbstd.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/cdc.h>

extern const struct usb_interface_descriptor usbserial_comm_iface[];
extern const struct usb_interface_descriptor usbserial_data_iface[];
extern const struct usb_iface_assoc_descriptor usbserial_assoc;

void usbserial_set_config(usbd_device *usbd_dev, uint16_t wValue __attribute((unused)));

StreamBufferHandle_t usbserial_stream();

#endif