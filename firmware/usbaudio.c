#include "usbaudio.h"

#include <FreeRTOS.h>
#include <stream_buffer.h>
#include <task.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/desig.h>
#include <libopencm3/cm3/common.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/audio.h>
#include <libopencm3/usb/dwc/otg_common.h>
#include <libopencm3/usb/dwc/otg_fs.h>

#include "audio.h"
#include "usbcomposite.h"

#define MIN(a,b)             ((a<b) ? a : b)

#define VENDOR_ID            0x0483  
#define PRODUCT_ID           0x5119  
#define DEVICE_VER           0x0200  

#define SAMPLE_RATE          8000
#define SAMPLE_BUFFER_N_SAMP 8
#define SAMPLE_BUFFER_SIZE   (sizeof(IQSample)*SAMPLE_BUFFER_N_SAMP)


static const struct usb_device_descriptor dev = {
    .bLength            = USB_DT_DEVICE_SIZE,  
    .bDescriptorType    = USB_DT_DEVICE, 
    .bcdUSB             = 0x0200,              //< USB 1.1
    .bDeviceClass       = 0,                   //< defined at interface level
    .bDeviceSubClass    = 0,                   //< defined at interface level
    .bDeviceProtocol    = 0,                   //< defined at interface level
    .bMaxPacketSize0    = 64,                  //< defined at interface level
    .idVendor           = VENDOR_ID,           //< VID
    .idProduct          = PRODUCT_ID,          //< PID
    .bcdDevice          = DEVICE_VER,          //< Device
    .iManufacturer      = 1,                   //< index to string desc
    .iProduct           = 2,                   //< index to string desc 
    .iSerialNumber      = 3,                   //< index to string desc
    .bNumConfigurations = 1,
};

static const struct {
    struct usb_audio_header_descriptor_head header_head;
    struct usb_audio_header_descriptor_body header_body;
    struct usb_audio_input_terminal_descriptor input_terminal_desc;
    struct usb_audio_feature_unit_descriptor_2ch feature_unit_desc;
    struct usb_audio_output_terminal_descriptor output_terminal_desc;
} __attribute__((packed)) audio_control_functional_descriptors = {
    .header_head = {
        .bLength = sizeof(struct usb_audio_header_descriptor_head) +
                   1 * sizeof(struct usb_audio_header_descriptor_body),
        .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
        .bDescriptorSubtype = USB_AUDIO_TYPE_HEADER,
        .bcdADC = 0x0100,
        .wTotalLength =
               sizeof(struct usb_audio_header_descriptor_head) +
               1 * sizeof(struct usb_audio_header_descriptor_body) +
               sizeof(struct usb_audio_input_terminal_descriptor) +
               sizeof(struct usb_audio_feature_unit_descriptor_2ch) +
               sizeof(struct usb_audio_output_terminal_descriptor),
        .binCollection = 1,
    },
    .header_body = {
        .baInterfaceNr = 0x01,
    },
    .input_terminal_desc = {
        .bLength = sizeof(struct usb_audio_input_terminal_descriptor),
        .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
        .bDescriptorSubtype = USB_AUDIO_TYPE_INPUT_TERMINAL,
        .bTerminalID = 1,
        .wTerminalType = 0x0710, /* Radio receiver */
        .bAssocTerminal = 0,
        .bNrChannels = 2,
        .wChannelConfig = 0x0003, /* Left & Right channels */
        .iChannelNames = 0,
        .iTerminal = 0,
    },
    .feature_unit_desc = {
        .head = {
            .bLength = sizeof(struct usb_audio_feature_unit_descriptor_2ch),
            .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
            .bDescriptorSubtype = USB_AUDIO_TYPE_FEATURE_UNIT,
            .bUnitID = 2,
            .bSourceID = 1, /* Input terminal 1 */
            .bControlSize = 2,
            .bmaControlMaster = 0x0001, /* 'Mute' is supported */
        },
        .channel_control = { {
            .bmaControl = 0x0000,
        }, {
            .bmaControl = 0x0000,
        } },
        .tail = {
            .iFeature = 0x00,
        }
    },
    .output_terminal_desc = {
        .bLength = sizeof(struct usb_audio_output_terminal_descriptor),
        .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
        .bDescriptorSubtype = USB_AUDIO_TYPE_OUTPUT_TERMINAL,
        .bTerminalID = 3,
        .wTerminalType = 0x0101, /* USB Streaming */
        .bAssocTerminal = 0,
        .bSourceID = 0x02, /* Feature unit 2 */
        .iTerminal = 0,
    }
};

const struct usb_interface_descriptor usbaudio_control_iface[] = {{
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = 0,
    .bAlternateSetting = 0,
    .bNumEndpoints = 0,
    .bInterfaceClass = USB_CLASS_AUDIO,
    .bInterfaceSubClass = USB_AUDIO_SUBCLASS_CONTROL,
    .bInterfaceProtocol = 0,
    .iInterface = 0,

    .extra = &audio_control_functional_descriptors,
    .extralen = sizeof(audio_control_functional_descriptors)
} };

static const struct {
    struct usb_audio_stream_interface_descriptor audio_cs_streaming_iface_desc;
    struct usb_audio_format_type1_descriptor_1freq audio_type1_format_desc;
} __attribute__((packed)) audio_streaming_functional_descriptors = {
    .audio_cs_streaming_iface_desc = {
        .bLength = sizeof(struct usb_audio_stream_interface_descriptor),
        .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
        .bDescriptorSubtype = 1, /* AS_GENERAL */
        .bTerminalLink = 3, /* Terminal 3 */
        .bDelay = 0,
        .wFormatTag = 0x0001 /* PCM Format */,
    },
    .audio_type1_format_desc = {
        .head = {
            .bLength = sizeof(struct usb_audio_format_type1_descriptor_1freq),
            .bDescriptorType = USB_AUDIO_DT_CS_INTERFACE,
            .bDescriptorSubtype = 2, /* FORMAT_TYPE */
            .bFormatType = 1,
            .bNrChannels = 2,
            .bSubFrameSize = 2,
            .bBitResolution = 16, /* Should be 10, but 16 is more reliable */
            .bSamFreqType = 1, /* 1 discrete sampling frequency */
        },
        .freqs = { {
            .tSamFreq = SAMPLE_RATE,
        } },
    }
};

static const struct usb_audio_stream_audio_endpoint_descriptor audio_streaming_cs_ep_desc[] = { {
    .bLength = sizeof(struct usb_audio_stream_audio_endpoint_descriptor),
    .bDescriptorType = USB_AUDIO_DT_CS_ENDPOINT,
    .bDescriptorSubtype = 1, /* EP_GENERAL */
    .bmAttributes = 0,
    .bLockDelayUnits = 0x02, /* PCM samples */
    .wLockDelay = 0x0000,
} };

static const struct usb_endpoint_descriptor isochronous_ep[] = { {
    .bLength = USB_DT_ENDPOINT_SIZE,
    .bDescriptorType = USB_DT_ENDPOINT,
    .bEndpointAddress = 0x82,
    .bmAttributes = USB_ENDPOINT_ATTR_ASYNC | USB_ENDPOINT_ATTR_ISOCHRONOUS,
    .wMaxPacketSize = 256,
    .bInterval = 0x01, /* 1 millisecond */

    /* not using usb_audio_stream_endpoint_descriptor??
     * (Why? These are USBv1.0 endpoint descriptors)*/

    .extra = &audio_streaming_cs_ep_desc[0],
    .extralen = sizeof(audio_streaming_cs_ep_desc[0])
} };

const struct usb_interface_descriptor usbaudio_streaming_iface[] = { {
    /* zero-bw streaming interface (alt 0) */
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = 1,
    .bAlternateSetting = 0,
    .bNumEndpoints = 0,
    .bInterfaceClass = USB_CLASS_AUDIO,
    .bInterfaceSubClass = USB_AUDIO_SUBCLASS_AUDIOSTREAMING,
    .bInterfaceProtocol = 0,
    .iInterface = 0,

    .extra = 0,
    .extralen = 0,
}, {
    /* actual streaming interface (alt 1) */
    .bLength = USB_DT_INTERFACE_SIZE,
    .bDescriptorType = USB_DT_INTERFACE,
    .bInterfaceNumber = 1,
    .bAlternateSetting = 1,
    .bNumEndpoints = 1,
    .bInterfaceClass = USB_CLASS_AUDIO,
    .bInterfaceSubClass = USB_AUDIO_SUBCLASS_AUDIOSTREAMING,
    .bInterfaceProtocol = 0,
    .iInterface = 0,

    .endpoint = isochronous_ep,

    .extra = &audio_streaming_functional_descriptors,
    .extralen = sizeof(audio_streaming_functional_descriptors)
} };



StreamBufferHandle_t _stream = NULL;

#define BUFFER_N_SAMPLES 8
#define BUFFER_SIZE      (sizeof(IQSample)*BUFFER_N_SAMPLES)
IQSample iq_buffer[BUFFER_N_SAMPLES];

#define USB_REBASE(x) MMIO32((x) + (USB_OTG_FS_BASE))
#define USB_DIEPCTLX_SD1PID     (1 << 29) /* Odd frames */
#define USB_DIEPCTLX_SD0PID     (1 << 28) /* Even frames */
void toggle_isochronous_frame(uint8_t ep)
{
    static int toggle = 0;
    if (toggle++ % 2 == 0) {
        USB_REBASE(OTG_DIEPCTL(ep)) |= USB_DIEPCTLX_SD0PID;
    } else {
        USB_REBASE(OTG_DIEPCTL(ep)) |= USB_DIEPCTLX_SD1PID;
    }
}

void usbaudio_iso_stream_callback(usbd_device *usbd_dev, uint8_t ep) {
    int n = MIN(xStreamBufferBytesAvailable(_stream)/sizeof(IQSample), BUFFER_N_SAMPLES);
    for (int i=0; i<n; i++) {
        xStreamBufferReceive(_stream, iq_buffer + i, sizeof(IQSample), 0);
    }
    toggle_isochronous_frame(ep);   
    usbd_ep_write_packet(usbd_dev, USBAUDIO_EP_LINEOUT_ADDR, iq_buffer, 2*n*sizeof(int16_t));
}

void usbaudio_set_config(usbd_device *usbd_dev, uint16_t wValue __attribute((unused))) {
    gpio_toggle(GPIOA, GPIO6);
    usbd_ep_setup(usbd_dev, USBAUDIO_EP_LINEOUT_ADDR, USB_ENDPOINT_ATTR_ISOCHRONOUS, BUFFER_SIZE, usbaudio_iso_stream_callback);
    usbd_ep_write_packet(usbd_dev, USBAUDIO_EP_LINEOUT_ADDR, iq_buffer, BUFFER_SIZE);
}

static void 
usbaudio_task(void *arg) {
    usbd_device *usbd_dev = (usbd_device *)(arg);

    for (;;) {
        usbd_poll(usbd_dev);
        taskYIELD();
    }
}


StreamBufferHandle_t 
usbaudio_stream() {
    return _stream;
}

void 
usbaudio_setup(usbd_device *usbd_dev) {
    _stream = xStreamBufferCreate(sizeof(IQSample)*BUFFER_N_SAMPLES, 2*sizeof(int16_t));

    xTaskCreate(usbaudio_task, "USB", 200, usbd_dev, 1, NULL);
}