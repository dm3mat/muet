#include "audio.h"

#include <FreeRTOS.h>
#include <stream_buffer.h>
#include <task.h>

#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dac.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>


StreamBufferHandle_t _iq_stream   = NULL;
StreamBufferHandle_t _hp_stream   = NULL;
StreamBufferHandle_t _mic_stream  = NULL;

typedef struct {
  uint32_t 
    iq_enabled  : 1,
    mic_enabled : 1,
    hp_enabled  : 1;
} AudioState;

volatile AudioState _audio_state = { 0 };

#define SAMPLE_RATE            8000
#define BUFFER_SAMPLES         8
#define MIC_STREAM_BUFFER_SIZE (sizeof(int16_t)*BUFFER_SAMPLES)
#define IQ_STREAM_BUFFER_SIZE  (sizeof(IQSample)*BUFFER_SAMPLES)
#define HP_STREAM_BUFFER_SIZE  (sizeof(int16_t)*BUFFER_SAMPLES)
#define PWM_PERIOD             5250

void audio_timer_setup() {
  rcc_periph_clock_enable(RCC_TIM2);
  rcc_periph_reset_pulse(RST_TIM2);

  timer_disable_counter(TIM2);
  // 168000000 / 4 / 50 / 210 = 4000 ?!?
  timer_set_mode(TIM2, TIM_CR1_CKD_CK_INT_MUL_4, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_set_prescaler(TIM2, 50); 
	timer_set_period(TIM2, 205);

  timer_set_master_mode(TIM2, TIM_CR2_MMS_UPDATE);

  timer_enable_counter(TIM2);
}


void pwm_setup() {
  rcc_periph_clock_enable(RCC_TIM4);
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_reset_pulse(RST_TIM4); 

  gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6);
  gpio_set_output_options(GPIOB, GPIO_OTYPE_PP, GPIO_OSPEED_50MHZ, GPIO6);
  gpio_set_af(GPIOB, GPIO_AF2, GPIO6);

  timer_enable_preload(TIM4);
  timer_continuous_mode(TIM4);
  timer_set_repetition_counter(TIM4, 0);
  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  timer_set_prescaler(TIM4, 2);
  timer_set_period(TIM4, 5250);
  
  timer_disable_oc_output(TIM4, TIM_OC1);
  timer_set_oc_mode(TIM4, TIM_OC1, TIM_OCM_PWM1);
  timer_enable_oc_preload(TIM4, TIM_OC1);
  timer_set_oc_value(TIM4, TIM_OC1, 0);
  timer_enable_oc_output(TIM4, TIM_OC1);
}


void 
adc_setup() {
  _iq_stream   = xStreamBufferCreate(IQ_STREAM_BUFFER_SIZE, sizeof(IQSample));
  _mic_stream  = xStreamBufferCreate(sizeof(int16_t)*IQ_STREAM_BUFFER_SIZE, sizeof(int16_t));

  rcc_periph_clock_enable(RCC_ADC1);
  rcc_periph_clock_enable(RCC_ADC2);
  rcc_periph_clock_enable(RCC_ADC3);
	rcc_periph_clock_enable(RCC_GPIOA);

  gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO0|GPIO1|GPIO2);

  adc_power_off(ADC1);
  adc_power_off(ADC2);
  adc_power_off(ADC3);

  adc_set_single_conversion_mode(ADC1);
  adc_set_single_conversion_mode(ADC2);
  adc_set_single_conversion_mode(ADC3);
  adc_set_multi_mode(ADC_CCR_MULTI_TRIPLE_REGULAR_SIMUL);
  
  adc_set_clk_prescale(ADC_CCR_ADCPRE_BY8);
  adc_set_sample_time(ADC1, ADC_CHANNEL0, ADC_SMPR_SMP_480CYC);
  adc_set_sample_time(ADC2, ADC_CHANNEL1, ADC_SMPR_SMP_480CYC);
  adc_set_sample_time(ADC3, ADC_CHANNEL2, ADC_SMPR_SMP_480CYC);

  adc_disable_scan_mode(ADC1);
  adc_disable_scan_mode(ADC2);
  adc_disable_scan_mode(ADC3);  

  uint8_t channel_array[1]; channel_array[0] = ADC_CHANNEL0;
  adc_set_regular_sequence(ADC1, 1, channel_array);
  channel_array[0] = ADC_CHANNEL1;
  adc_set_regular_sequence(ADC2, 1, channel_array);
  channel_array[0] = ADC_CHANNEL2;
  adc_set_regular_sequence(ADC3, 1, channel_array);
    
  adc_enable_external_trigger_regular(ADC1, ADC_CR2_EXTSEL_TIM2_TRGO, ADC_CR2_EXTEN_RISING_EDGE);
  
  adc_enable_eoc_interrupt(ADC1);

  adc_power_on(ADC1);
  adc_power_on(ADC2);
  adc_power_on(ADC3); 

  nvic_set_priority(NVIC_ADC_IRQ, 0);
  nvic_enable_irq(NVIC_ADC_IRQ);
}


void 
dac_setup() {
  _hp_stream = xStreamBufferCreate(sizeof(int16_t)*IQ_STREAM_BUFFER_SIZE, sizeof(int16_t));

  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_DAC);
  gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO4);

  dac_disable(DAC1, DAC_CHANNEL_BOTH);
  dac_disable_waveform_generation(DAC1, DAC_CHANNEL1);
  dac_set_trigger_source(DAC1, DAC_CR_TSEL1_T2);
  dac_trigger_enable(DAC1, DAC_CHANNEL1);
  dac_buffer_enable(DAC1, DAC_CHANNEL1);

  dac_enable(DAC1, DAC_CHANNEL1);
}


void
audio_setup() 
{
  audio_iq_stop();
  audio_mic_stop();
  audio_hp_stop();
  audio_pwm_set(0);

  adc_setup();
  dac_setup();
  pwm_setup();

  audio_timer_setup();
}


StreamBufferHandle_t 
audio_iq_stream() {
  return _iq_stream;
}

void 
audio_iq_start() {
  _audio_state.iq_enabled = 1;
}

void 
audio_iq_stop() {
  _audio_state.iq_enabled = 0;
}


StreamBufferHandle_t
audio_hp_stream() {
  return _hp_stream;
}

void 
audio_hp_start() {
  _audio_state.hp_enabled = 1;
}

void 
audio_hp_stop() {
  _audio_state.hp_enabled = 0;
}


StreamBufferHandle_t
audio_mic_stream() {
  return _mic_stream;
}

void 
audio_mic_start() {
  _audio_state.mic_enabled = 1;
}

void 
audio_mic_stop() {
  _audio_state.mic_enabled = 0;
}

void 
audio_pwm_set(uint16_t value) {
  timer_set_oc_value(TIM4, TIM_OC1, (((uint32_t) value)*PWM_PERIOD)/65535);
}

void 
audio_pwm_enable() { 
  timer_enable_counter(TIM4);
}

void 
audio_pwm_disable() { 
  timer_disable_counter(TIM4);
}


void adc_isr() {
  BaseType_t higherPriorityTaskWoken = pdFALSE; 

  if (_audio_state.iq_enabled && adc_eoc(ADC1) && adc_eoc(ADC2)) {
    IQSample sample;
    sample.i = ADC_DR(ADC1);
    sample.q = ADC_DR(ADC2);
    xStreamBufferSendFromISR(_iq_stream, (void *) &sample, sizeof(IQSample), &higherPriorityTaskWoken);
  } 
    
  if (_audio_state.mic_enabled && adc_eoc(ADC3)) {
    int16_t value = ADC_DR(ADC3);
    xStreamBufferSendFromISR(_mic_stream, (void *) &value, sizeof(int16_t), &higherPriorityTaskWoken);
  }

  if (_audio_state.hp_enabled && xStreamBufferBytesAvailable(_hp_stream)) {
    int16_t value;
    if (xStreamBufferReceiveFromISR(_hp_stream, &value, sizeof(int16_t), &higherPriorityTaskWoken))
      dac_load_data_buffer_single(DAC1, value, DAC_ALIGN_RIGHT12, DAC_CHANNEL1);
  }

  portEND_SWITCHING_ISR(higherPriorityTaskWoken);
}



