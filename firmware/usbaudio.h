#ifndef __USBAUDIO_H__
#define __USBAUDIO_H__

#include <FreeRTOS.h>
#include <stream_buffer.h>
#include <libopencm3/usb/usbstd.h>
#include <libopencm3/usb/usbd.h>

/** The USB interface descriptor for the control interface to the USB sound-card. */
extern const struct usb_interface_descriptor usbaudio_control_iface[];
/** The USB interface descriptor for the data-stream interface to the USB sound-card. */
extern const struct usb_interface_descriptor usbaudio_streaming_iface[];

void usbaudio_set_config(usbd_device *usbd_dev, uint16_t wValue __attribute((unused)));

void usbaudio_setup(usbd_device *usbd_dev);

/** Returns the line-out (TRX->PC) audio stream. USB audio interface must be setup first, by 
 * calling @c usbaudio_setup. */
StreamBufferHandle_t usbaudio_lineout_stream();


#endif