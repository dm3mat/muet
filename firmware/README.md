# mÜT Firmware
This directory contains the firmware of the device. It is build upon the HAL libopencm3 as well as 
FreeRTOS. 

Once completed, the firmware knows three modes of operation:
  1. Headless IQ mode. In this mode, the device provides two audio interfaces. One stereo line-in
     interface, proving the raw IQ samples from the receiver and an stereo line-out interface 
     expecting an IQ audio signal to transmit. The latter already implements the modulation at the
     PC side. Only providing the IQ signal to the PC, also requires the PC to implement the 
     demodulation. Additionally, the device provides a CAT (serial) interface to controll the 
     radio. I.e. its frequency and PTT. Mode settings do not affect this operation, as the 
     modulation/demodulation happens at the PC side anyway.
     
  2. Headless TRX mode. Also provides two audio interfaces (mono line-in and line-out) to the PC. 
     In contrast to the previous mode, this one expects and provides plain audio signals. The 
     modulation and demodulation happens at the MCU and not at the PC. Additionally, the CAT 
     interface remains present. To this end, the device behaves more like a traditional TRX and
     thus can be interfaced with common digi-mode software.

  3. Full TRX mode. This mode requires some additional hardware like a display, knobs and audio 
     pre-amps and PAs. This hardware will be designed later as a "head" to the TRX, which allows
     the device to be used like a normal stand-alone TRX.

Providing these interfaces may appear overkill, but in fact, from the firmware perspective, they 
nicely fit into eachother. For example, when providing operational mode 2 (headless TRX), the input 
for the final demodulation step, will be the IQ samples from the RX mixers. At the same time, to
compute the instantaneous phase and amplitude passed to the PA stage of the signal to be 
transmitted, requires a Hilberttransform. The latter is just the Q of an I signal. To this end, 
operational mode 1 could be seen as the SDR core of the TRX upon which, operational mode 2 is 
implemented. Finally, the full TRX mode then build upon this abstraction layer provided by the 
headless TRX mode.

## Dependencies:
  1. Make sure, to klone the submudoules too. If not yet done, run `git submodule update --init` 
  once.
  2. Install gcc for arm, that is gcc-arm-none-eabi
  3. Build libopencm3 under `firmware/libopencm3/` using `make TARGETS='stm32/f4'` there.
  4. A tool to flash the firmware via JTAG, e.g. `st-flash` from `stlink-tools`.

