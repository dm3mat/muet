#include "usbcomposite.h"
#include "usbserial.h"
#include "usbaudio.h"

#include <FreeRTOS.h>
#include <task.h>

#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/cdc.h>
#include <libopencm3/usb/dwc/otg_fs.h>
#include <libopencm3/stm32/desig.h>


static const struct usb_device_descriptor dev = {
  .bLength            = USB_DT_DEVICE_SIZE,
  .bDescriptorType    = USB_DT_DEVICE,
  .bcdUSB             = 0x0200,
  .bDeviceClass       = 0,
  .bDeviceSubClass    = 0,
  .bDeviceProtocol    = 0,
  .bMaxPacketSize0    = 64,
  .idVendor           = 0x05ac, // VID
  .idProduct          = 0x2227,	// PID
  .bcdDevice          = 0x0200,	// Version (2.00)
  .iManufacturer      = 1,
  .iProduct           = 2,
  .iSerialNumber      = 3,
  .bNumConfigurations = 1,
};

uint8_t usbaudio_streaming_iface_cur_altsetting = 0;

static const struct usb_interface ifaces[] = {{
	.num_altsetting = 1,
	.altsetting     = usbserial_comm_iface,
}, {
	.num_altsetting = 1,
	.altsetting     = usbserial_data_iface,
}, {
    .num_altsetting = 1,
    .altsetting     = usbaudio_control_iface,
}, {
    .num_altsetting = 2,
    .cur_altsetting = &usbaudio_streaming_iface_cur_altsetting,
    .altsetting     = usbaudio_streaming_iface,
} };

static const struct usb_config_descriptor config = {
  .bLength             = USB_DT_CONFIGURATION_SIZE,
  .bDescriptorType     = USB_DT_CONFIGURATION,
  .wTotalLength        = 0, /* can be anything, it is updated automatically
                               when the usb code prepares the descriptor */
  .bNumInterfaces      = 4, /* control and streaming */
  .bConfigurationValue = 1,
  .iConfiguration      = 0,
  .bmAttributes        = 0x80, /* bus powered */
  .bMaxPower           = 0x32,
  .interface           = ifaces,
};

static char usb_serial_number[13];	// 12 digits plus a null terminator
static const char * usb_strings[] = {
  "DM3MAT",
  "Audio + CAT",
  usb_serial_number
};

static void usbcomposite_set_config(usbd_device *usbd_dev, uint16_t wValue) {
  usbaudio_set_config(usbd_dev, wValue);
  usbserial_set_config(usbd_dev, wValue);
}

/* Buffer to be used for control requests. */
uint8_t usbd_control_buffer[128];

void 
usbcomposite_setup(void) {
  rcc_periph_clock_enable(RCC_OTGFS);
  
  /* USB pins */
  gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO11 | GPIO12);
  gpio_set_af(GPIOA, GPIO_AF10, GPIO11 | GPIO12);

  desig_get_unique_id_as_string(usb_serial_number, sizeof(usb_serial_number));
  usbd_device *usbd_dev = usbd_init(&otgfs_usb_driver, &dev, &config,
    usb_strings, 3, usbd_control_buffer, sizeof(usbd_control_buffer));

  // Fix libopencm3 to work without VBUS sense
  OTG_FS_GCCFG |= OTG_GCCFG_NOVBUSSENS | OTG_GCCFG_PWRDWN;
  OTG_FS_GCCFG &= ~(OTG_GCCFG_VBUSBSEN | OTG_GCCFG_VBUSASEN);

  usbd_register_set_config_callback(usbd_dev, usbcomposite_set_config);
}