#ifndef __AUDIO_H__
#define __AUDIO_H__

#include <FreeRTOS.h>
#include <stream_buffer.h>

typedef struct __attribute__((packed)) {
  int16_t i;
  int16_t q;
} IQSample;

void audio_setup();

StreamBufferHandle_t audio_iq_stream();
void audio_iq_start();
void audio_iq_stop();

StreamBufferHandle_t audio_mic_stream();
void audio_mic_start();
void audio_mic_stop();

StreamBufferHandle_t audio_hp_stream(); 
void audio_hp_start();
void audio_hp_stop();

void audio_pwm_set(uint16_t value);
void audio_pwm_enable();
void audio_pwm_disable();

#endif