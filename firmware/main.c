#include <FreeRTOS.h>
#include <task.h>

#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

//#include "usbserial.h"
#include "audio.h"
#include "usbcomposite.h"
#include "audiotask.h"


void 
vApplicationStackOverflowHook(
  TaskHandle_t pxTask __attribute((unused)), 
  char *pcTaskName __attribute((unused))) 
{
    gpio_toggle(GPIOA, GPIO6);
    for (uint32_t i=0; i<250000; i++) {
      __asm__("nop");
    }
    gpio_set(GPIOA, GPIO6);
    for (uint32_t i=0; i<1000000; i++) {
      __asm__("nop");
    }
}

static void 
gpio_setup(void) {
  // Wnable GPIOA clock
  rcc_periph_clock_enable(RCC_GPIOA);
  // PA6 -> output, push-pull
  gpio_mode_setup(
        GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO6);
}


int 
main(void) 
{
  rcc_clock_setup_pll(&rcc_hse_8mhz_3v3[RCC_CLOCK_3V3_168MHZ]);

  gpio_setup();
  audio_setup();
  usbcomposite_setup();
  
  audio_iq_start();
  audio_hp_start();
  audio_pwm_enable();
  
  xTaskCreate(audiotask, "audio", 200, NULL, configMAX_PRIORITIES-1, NULL);
  vTaskStartScheduler();
}

