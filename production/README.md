# Production files

This folder contains the production files (i.e., Gerber files, placement etc.) for a particular 
revision of the hardware. Each revision is tagged, so if you need some board in a particular 
revision, checkout the corresponding tag first.