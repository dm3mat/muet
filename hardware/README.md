# Circuit

This directory contains the KiCAD files for the circuit and boards. In a first step, we develop the
single components that form the final circuit like amplifiers, filters, mixers etc. as separate 
modules once, each sub-circuit is confirmed to work, we combine them into a single board.

## Sub-circuits/Modules
There are several sub-circuits and modules:

   1. `BandpassModule.kicad_pro` -- Project file for the band-pass filter. This project uses the 
      shared `bandpass.kicad_sch` schematic.
   2. `RXModule.kicad_pro` -- Project file for the mixer, phase-shifter and audio pre-amp. This 
      project uses `RX.kicad_sch` schematic.
  
Further modules are added as needed. Finally, the transceiver circuit is then combined in a 
separate project that includes all these sub-circuits.