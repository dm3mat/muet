\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{handbook}[%
    2022/04/19
    v0.1
    Class for electronic project documentation.]

\newcommand{\showEditorialNotes}{hide}
\DeclareOption{draft}{
 \renewcommand{\showEditorialNotes}{show}
}
\ProcessOptions\relax

\LoadClass[a4paper,parskip=half,11pt,titlepage]{scrartcl}
\RequirePackage[utf8]{inputenc}
\RequirePackage{tcolorbox}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsthm}
\RequirePackage{multicol}
\RequirePackage[hidelinks]{hyperref}
\RequirePackage{graphics}
\RequirePackage{standalone}
\RequirePackage{geometry}
\RequirePackage{circuitikz}

\usetikzlibrary{positioning, fit, calc}

\newenvironment{warningbox}{\begin{tcolorbox}[colback=red!5!white,colframe=red!75!black,title=Warning]
}{\end{tcolorbox}}
\newenvironment{notebox}{\begin{tcolorbox}[colback=blue!5!white,colframe=blue!75!black,title=Note]
}{\end{tcolorbox}}